# Easymeet

A simple app to generate a form to facilitate your note taking during business meetings.

Go checkout [easymeet.gitlab.io](https://easymeet.gitlab.io)!

## Features

- A space to **staple business cards** or fill out infos about your contact.
- Event and date can be filled out or left blank.
- **Customizable** "Interested in" and "Contact in" parts to fit your needs.
- A large space to write text.
- 3 formats : A5, A4 and double A5.
