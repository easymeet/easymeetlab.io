import React, { Component } from "react";
import "./Radio.scss";

export class Radio extends Component {
  render() {
    const { text, checked, onChange } = this.props;
    return (
      <label className="radio-list-item">
        {text}
        <input
          type="radio"
          name="radio"
          onChange={onChange}
          checked={checked}
        />
        <span className="checkmark" />
      </label>
    );
  }
}

export class RadioList extends Component {
  render() {
    const { items, onChange, selected } = this.props;
    return (
      <div className="radio-list">
        {items.map(element => (
          <Radio
            key={element}
            text={element}
            checked={selected === element}
            onChange={() => onChange(element)}
          />
        ))}
      </div>
    );
  }
}
