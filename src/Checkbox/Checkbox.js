import React, { Component } from 'react';
import './Checkbox.scss'

export class CheckboxList extends Component {
  render() {
    return (
      <div className="checkbox-list">
        {this.props.children}
      </div>
    );
  }
}

export class Checkbox extends Component {
  render() {
    const { text, checked, onClick } = this.props;
    return (
      <label className="checkbox-list-item">
        {text}
        <input type="checkbox" checked={checked} onChange={onClick}/>
        <span className="checkmark"></span>
      </label>
    );
  }
}