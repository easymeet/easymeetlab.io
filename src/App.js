import React, { Component } from "react";
import "./App.scss";
import { FormPreview } from "./Form/Form";
import { CheckboxList, Checkbox } from "./Checkbox/Checkbox";
import { RadioList } from "./Radio/Radio";
import { TextInput } from "./TextInput/TextInput";
import { AddForm } from "./AddForm/AddForm";
import logo from "./logo.svg";

class App extends Component {
  state = {
    event: "",
    date: "",
    interestedIn: ["Engineering", "Training", "Support"],
    interestedInSelected: ["Engineering", "Training", "Other"],
    contactIn: [
      "1 week",
      "2 weeks",
      "1 month",
      "2 months",
      "3 months",
      "6 months",
      "9 months",
      "1 year"
    ],
    contactInSelected: [
      "1 week",
      "2 weeks",
      "1 month",
      "3 months",
      "6 months",
      "1 year"
    ],
    format: ["A5", "A4", "Double A5"],
    formatSelected: "Double A5"
  };

  handleEvent = element => {
    this.setState({ event: element });
  };

  handleDate = element => {
    this.setState({ date: element });
  };

  handleInterestedIn = element => {
    let { interestedInSelected } = this.state;
    if (interestedInSelected.indexOf(element) >= 0) {
      interestedInSelected = interestedInSelected.filter(
        item => item !== element
      );
    } else {
      interestedInSelected.push(element);
    }
    this.setState({ interestedInSelected });
  };

  handleContactIn = element => {
    let { contactInSelected } = this.state;
    if (contactInSelected.indexOf(element) >= 0) {
      contactInSelected = contactInSelected.filter(item => item !== element);
    } else {
      contactInSelected.push(element);
    }
    this.setState({ contactInSelected });
  };

  handleFormat = format => {
    let { formatSelected } = this.state;
    formatSelected = format;
    this.setState({ formatSelected });
  };

  handleAddTopic = element => {
    let { interestedIn, interestedInSelected } = this.state;
    if (interestedIn.indexOf(element) === -1) {
      interestedIn.push(element);
      interestedInSelected.push(element);
    }
    this.setState({ interestedIn });
  };

  render() {
    const {
      event,
      date,
      interestedIn,
      interestedInSelected,
      contactIn,
      contactInSelected,
      format,
      formatSelected
    } = this.state;
    const interestedInVariable = [...interestedIn, "Other"];

    return (
      <main>
        <div className="page">
          <div className="settings no-print">
            <div className="settings-title">
              <img src={logo} className="settings-logo" alt="logo" />
              <h1>{"easymeet"}</h1>
            </div>
            <h2>{"Facilitate your note taking during business meetings"}</h2>

            <h3>{"Format"}</h3>
            <RadioList
              items={format}
              selected={formatSelected}
              onChange={this.handleFormat}
            />

            <h3>{"Event"}</h3>
            <TextInput
              text={event}
              placeholder={"The event name"}
              onChange={this.handleEvent}
            />

            <h3>{"Date"}</h3>
            <TextInput
              text={date}
              placeholder={"The event date"}
              onChange={this.handleDate}
            />

            <h3>{"Interested in"}</h3>
            <CheckboxList>
              {interestedInVariable.map(element => (
                <Checkbox
                  key={element}
                  text={element}
                  onClick={() => this.handleInterestedIn(element)}
                  checked={interestedInSelected.indexOf(element) >= 0}
                />
              ))}
            </CheckboxList>

            <AddForm onAddTopic={this.handleAddTopic} />

            <h3>{"Contact in"}</h3>
            <CheckboxList>
              {contactIn.map(element => (
                <Checkbox
                  key={element}
                  text={element}
                  onClick={() => this.handleContactIn(element)}
                  checked={contactInSelected.indexOf(element) >= 0}
                />
              ))}
            </CheckboxList>
          </div>
          <div className="preview">
            <FormPreview
              event={event}
              date={date}
              interestedIn={interestedInVariable}
              interestedInSelected={interestedInSelected}
              contactIn={contactIn}
              contactInSelected={contactInSelected}
              formatSelected={formatSelected}
            />
          </div>
        </div>

        <footer className="no-print">
          <a href="http://easymeet.gitlab.io">{"easymeet.gitlab.io"}</a>
          {" by "}
          <a href="http://easymov.fr" target="blank">
            {"Easymov Robotics"}
          </a>
        </footer>
      </main>
    );
  }
}

export default App;
