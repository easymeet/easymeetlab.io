import React, { Component } from "react";
import "./TextInput.scss";

export class TextInput extends Component {
  handleChange = event => {
    this.props.onChange(event.target.value);
  };

  render() {
    const { text, placeholder } = this.props;
    return (
      <div className="text-input">
        <input
          type="text"
          id="name"
          name="name"
          placeholder={placeholder}
          size="10"
          value={text}
          onChange={this.handleChange}
        />
      </div>
    );
  }
}
