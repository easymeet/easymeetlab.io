import React, { Component } from 'react';
import './AddForm.scss'

export class AddForm extends Component {

  state = {
    text: ''
  }

  handleChange = evt => {
    this.setState({text: evt.target.value});
  }

  handleAdd = () => {
    if (this.state.text.length > 0) {
      this.props.onAddTopic(this.state.text)
      this.setState({text: ''})
    }
  }

  render() {
    return (
      <div className="add-form">
        <input onChange={this.handleChange} value={this.state.text} type="text" id="name" name="name" required/>
        <button onClick={this.handleAdd}><i className="fas fa-plus"></i></button>
      </div>
    );
  }
}