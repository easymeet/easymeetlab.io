import React, { Component } from "react";
import "./Form.scss";

class Checkbox extends Component {
  render() {
    return (
      <div className="checkbox">
        <div className="checkbox-case" />
        <div className="checkbox-text">{this.props.children}</div>
      </div>
    );
  }
}

class Block extends Component {
  render() {
    return (
      <div className={`block block-line-${this.props.lines}`}>
        {this.props.children}
      </div>
    );
  }
}

class MultiBlock extends Component {
  render() {
    return <div className={`multi-block`}>{this.props.children}</div>;
  }
}

class Title extends Component {
  render() {
    return <span className="title">{this.props.children}</span>;
  }
}

class Field extends Component {
  render() {
    return (
      <div className={`field block block-line-${this.props.lines}`}>
        <div className="field-form">
          <span className="block-field">{this.props.title}</span>
          {". ".repeat(2000)}
        </div>
        <div className={`field-writing`}>
          <span className="field-writing-title">{this.props.title}</span>
          <span className="field-writing-value">{this.props.value}</span>
        </div>
      </div>
    );
  }
}

class EmptyField extends Component {
  render() {
    return (
      <>
        <span className="block-field">{this.props.children}</span>
        {". ".repeat(2000)}
      </>
    );
  }
}

class TitleField extends Component {
  render() {
    return (
      <>
        <span className="title">{this.props.children}</span>
        {". ".repeat(4000)}
      </>
    );
  }
}

class Footer extends Component {
  render() {
    return (
      <div className="form-footer">
        <span className="footer-1">{"easymeet.gitlab.io "}</span>
        <span>{"by "}</span>
        <span className="footer-1">{"Easymov Robotics"}</span>
      </div>
    );
  }
}

export class FormContent extends Component {
  render() {
    const {
      event,
      date,
      interestedIn,
      interestedInSelected,
      contactIn,
      contactInSelected
    } = this.props;
    let interestedInFiltered = [];
    interestedIn.forEach(element => {
      if (interestedInSelected.indexOf(element) >= 0) {
        interestedInFiltered.push(element);
      }
    });

    let contactInFiltered = [];
    contactIn.forEach(element => {
      if (contactInSelected.indexOf(element) >= 0) {
        contactInFiltered.push(element);
      }
    });

    return (
      <>
        <div className="form-contact">
          <Block lines={1}>
            <Title>{"Business card / Contact information"}</Title>
          </Block>
          <Block lines={1}>
            <EmptyField>{"Company :"}</EmptyField>
          </Block>
          <Block lines={1}>
            <EmptyField>{"Name :"}</EmptyField>
          </Block>
          <Block lines={1}>
            <EmptyField>{"Position :"}</EmptyField>
          </Block>
          <Block lines={3}>
            <EmptyField>{"Contact :"}</EmptyField>
          </Block>
        </div>

        <div className="form-event">
          <Field title={"Event :"} value={event} lines={2} />
          <Field title={"Date :"} value={date} lines={1} />
        </div>

        <div className="form-content">
          {interestedInFiltered.length > 0 && (
            <MultiBlock>
              <Title>{"Interested in :"}</Title>
              {interestedInFiltered.map(element => {
                if (element === "Other") {
                  return (
                    <Checkbox key={element}>
                      {"Other: "}
                      {". ".repeat(30)}
                    </Checkbox>
                  );
                } else {
                  return <Checkbox key={element}>{element}</Checkbox>;
                }
              })}
            </MultiBlock>
          )}

          {contactInFiltered.length > 0 && (
            <MultiBlock>
              <Title>{"Contact in :"}</Title>
              {contactInFiltered.map(element => (
                <Checkbox key={element}>{element}</Checkbox>
              ))}
            </MultiBlock>
          )}

          <Block lines={60}>
            <TitleField>{"Information :"}</TitleField>
          </Block>
        </div>

        <Footer />
      </>
    );
  }
}

export class Print extends Component {
  render() {
    var isChrome =
      !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

    return (
      <div className="print no-print" onClick={window.print}>
        {isChrome ? (
          <div className="print-info">
            <i className="fas fa-print" />
            <span className="print-title">{"Print Me"}</span>
          </div>
        ) : (
          <div className="print-info print-warning">
            <i className="fas fa-exclamation-triangle" />
            <span className="print-text">
              {"This application has only been tested with "}
              <strong>{"Chrome"}</strong>
              {", all other browsers may not support it."}
            </span>
            <span className="print-title">{"Print Me Anyway"}</span>
          </div>
        )}
      </div>
    );
  }
}

export class Form extends Component {
  render() {
    const { formatClassName, formatSelected } = this.props;
    return (
      <div className={`form ${formatClassName}`}>
        <style>
          {`@media print {
          @page {
              margin: 0;
              size: ${formatSelected} portrait;
          }
        }`}
        </style>
        <Print />
        <FormContent {...this.props} />
      </div>
    );
  }
}

export class DoubleForm extends Component {
  render() {
    return (
      <div className={`form double-a5`}>
        <style>
          {`@media print {
          @page {
              margin: 0;
              size: A4 landscape;
          }
        }`}
        </style>
        <Print />
        <div className="before-rotate">
          <div className="rotate">
            <FormContent {...this.props} />
          </div>
          <div className="rotate2">
            <FormContent {...this.props} />
          </div>
        </div>
      </div>
    );
  }
}

export class FormPreview extends Component {
  render() {
    const { formatSelected } = this.props;
    const classFormat = {
      A5: "a5",
      A4: "a4",
      "Double A5": "double-a5"
    };
    const formatClassName = classFormat[formatSelected];
    return (
      <>
        {formatSelected === "A5" && (
          <Form {...this.props} formatClassName={formatClassName} />
        )}
        {formatSelected === "A4" && (
          <Form {...this.props} formatClassName={formatClassName} />
        )}
        {formatSelected === "Double A5" && <DoubleForm {...this.props} />}
      </>
    );
  }
}
